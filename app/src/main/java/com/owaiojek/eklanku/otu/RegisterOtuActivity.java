package com.owaiojek.eklanku.otu;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.owaiojek.eklanku.R;
import com.owaiojek.eklanku.api.OtuApi;
import com.owaiojek.eklanku.api.service.OtuService;
import com.owaiojek.eklanku.model.OtuResponse;
import com.owaiojek.eklanku.model.RegisterOtu;
import com.owaiojek.eklanku.model.ResponseRegisterOtu;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;


public class RegisterOtuActivity extends AppCompatActivity {

    @BindView(R.id.btnRegisterOtu)
    Button btnRegisterOtu;
    @BindView(R.id.txtNama)
    EditText txtNama;
    @BindView(R.id.txtPassword)
    EditText txtPassword;
    @BindView(R.id.txtPin)
    EditText txtPin;
    @BindView(R.id.txtUplineID)
    EditText txtUplineID;
    @BindView(R.id.txtUserID)
    EditText txtUserID;
    @BindView(R.id.txtEmail)
    EditText txtEmail;
    String valueNama,valuePassword,valuePin,valueUplineID,valueUserID,valueEmail;
    OtuService otuService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_otu);
        ButterKnife.bind(this);
        otuService = OtuApi.getClient().create(OtuService.class);

        btnRegisterOtu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valueNama =  txtNama.getText().toString();
                valuePassword = txtPassword.getText().toString();
                valuePin = txtPin.getText().toString();
                valueUplineID = txtUplineID.getText().toString();
                valueUserID = txtUserID.getText().toString();
                valueEmail = txtEmail.getText().toString();
                RegisterOtu registerOtu = new RegisterOtu("081336595339","EKL0000000","OWAY",
                        "Hadi Jar ","hadi@gmail.com","3332221dc","342433re");
//                Call<OtuResponse> users = otuService.registerOtu("222",registerOtu);
//                users.enqueue(new Callback<OtuResponse>() {
//                    @Override
//                    public void onResponse(Call<OtuResponse> call, retrofit2.Response<OtuResponse> response) {
//                        if (response.isSuccessful()) {
//                            Toast.makeText(RegisterOtuActivity.this, "Pemesanan dibatalkan", Toast.LENGTH_SHORT).show();
//
//                        } else {
////                            Log.d("isiku", "onResponse: "+response.body().getStatus().toString());
//                            Toast.makeText(RegisterOtuActivity.this, "COba", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<OtuResponse> call, Throwable t) {
//                        Log.e("TAG", "onFailure: " + t.toString());
//                        t.printStackTrace();
//                        Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
//                    }
//                });
//                registerOtu(registerOtu);
//                registerOtu();

            }
        });
    }
    private void registerOtu(RegisterOtu registerOtu){
//        Call<ResponseRegisterOtu> users = otuService.registerOtu("222",registerOtu);

//        Call<ResponseRegisterOtu> users = otuService.registerOtu("222",map);


    }

}
