package com.owaiojek.eklanku.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Iko Wirya on 4/10/2019.
 */
public class OtuResponse {
    @SerializedName("status")
    private String status;

    public OtuResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
