package com.owaiojek.eklanku.api;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Iko Wirya on 4/4/2019.
 */
public class OtuApi {
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {

        //interval koneksi
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor)

//        OkHttpClient client = new OkHttpClient.Builder()
                .build();

        //koneksi
        retrofit = new Retrofit.Builder()
                .baseUrl("https://api.eklanku.com/developapi/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit;

    }

}
