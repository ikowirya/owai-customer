package com.owaiojek.eklanku.api.service;


import com.owaiojek.eklanku.model.OtuResponse;
import com.owaiojek.eklanku.model.RegisterOtu;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
/**
 * Created by Iko Wirya on 4/4/2019.
 */
public interface OtuService {

//    @Headers("X-API-KEY: " + "222")
    @POST("Member/go_register")
    Call<OtuResponse> registerOtu(@Header ("X-API-KEY") @Body RegisterOtu registerOtu);
}
